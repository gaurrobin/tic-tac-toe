const DEFAULT_PLAYER_NAME = {
    PLAYER_ONE: "Player 1",
    PLAYER_TWO: "Player 2",
    COMPUTER: "Computer",
};

const PLAYING_WITH = {
    COMPUTER: "playing-with-computer",
    ANOTHER_PLAYER: "play-with-another-player",
};

const RESULT = {
    PLAYER_ONE_WIN: "player-one-win",
    PLAYER_TWO_WIN: "player-two-win",
    GAME_DRAW: "game-draw",
};

const MATRIX = {
    NONE: 0,
    PLAYER_ONE: 1,
    PLAYER_TWO: 2,
};

const COLOR = {
    NONE: "white",
    PLAYER_ONE: "red",
    PLAYER_TWO: "blue",
};

export { DEFAULT_PLAYER_NAME, PLAYING_WITH, RESULT, MATRIX, COLOR };
