import {
    Box,
    Button,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
} from "@material-ui/core";
import React from "react";
import {
    COLOR,
    DEFAULT_PLAYER_NAME,
    PLAYING_WITH,
} from "../../constants/Constant";

export default class PlayersInfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            playerOneName: DEFAULT_PLAYER_NAME.PLAYER_ONE,
            playerTwoName: DEFAULT_PLAYER_NAME.COMPUTER,
            playingWith: PLAYING_WITH.COMPUTER,
            isPlayerInfoEditable: true,
        };
    }

    setPlayerOneName = ({ target = {} }) => {
        const { value = "" } = target;
        if (value) {
            this.setState({ playerOneName: value });
        }
    };

    setPlayerTwoName = ({ target = {} }) => {
        const { value = "" } = target;
        if (value) {
            this.setState({ playerTwoName: value });
        }
    };

    setPlayingWith = ({ target = {} }) => {
        const { value = "" } = target;
        if (value === PLAYING_WITH.COMPUTER) {
            this.setState({
                playingWith: value,
                playerTwoName: DEFAULT_PLAYER_NAME.COMPUTER,
            });
        } else if (value === PLAYING_WITH.ANOTHER_PLAYER) {
            this.setState({
                playingWith: value,
                playerTwoName: DEFAULT_PLAYER_NAME.PLAYER_TWO,
            });
        }
    };

    onButtonClick = () => {
        const { onPlay, onEdit } = this.props;
        const {
            playerOneName,
            playerTwoName,
            playingWith,
            isPlayerInfoEditable,
        } = this.state;

        if (isPlayerInfoEditable) {
            this.setState({ isPlayerInfoEditable: !isPlayerInfoEditable });
            onPlay({ playerOneName, playerTwoName, playingWith });
        } else {
            this.setState({ isPlayerInfoEditable: !isPlayerInfoEditable });
            onEdit({ playerOneName, playerTwoName, playingWith });
        }
    };

    validatePlayerName = () => {
        const { playerOneName, playerTwoName } = this.state;
        if (playerOneName === playerTwoName) {
            return "Both players have same name";
        }
        return "";
    };

    isButtonDisable = () => {
        return !!this.validatePlayerName();
    };

    getPlayerOneInfoComponent = (data) => {
        const { playerOneName, isPlayerInfoEditable } = this.state;
        const { playerNameErrorMessage } = data;

        return (
            <Box margin={1}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="flex-start"
                >
                    <Box
                        width={30}
                        height={30}
                        bgcolor={COLOR.PLAYER_ONE}
                        marginRight={1}
                        marginTop={1.8}
                    />
                    <TextField
                        id="player-one-text-field"
                        label="Player 1 Name"
                        variant="outlined"
                        value={playerOneName}
                        onChange={this.setPlayerOneName}
                        disabled={!isPlayerInfoEditable}
                        error={!!playerNameErrorMessage}
                        helperText={playerNameErrorMessage}
                    />
                </Grid>
            </Box>
        );
    };

    getPlayerTwoInfoComponent = (data) => {
        const { playerTwoName, playingWith, isPlayerInfoEditable } = this.state;
        const { playerNameErrorMessage } = data;

        return (
            <Box margin={1}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="flex-start"
                >
                    <Box
                        width={30}
                        height={30}
                        bgcolor={COLOR.PLAYER_TWO}
                        marginRight={1}
                        marginTop={1.8}
                    />
                    <TextField
                        id="player-two-text-field"
                        label={
                            playingWith === PLAYING_WITH.COMPUTER
                                ? "Computer Name"
                                : "Player 2 Name"
                        }
                        variant="outlined"
                        value={playerTwoName}
                        onChange={this.setPlayerTwoName}
                        disabled={
                            !isPlayerInfoEditable ||
                            playingWith === PLAYING_WITH.COMPUTER
                        }
                        error={
                            playingWith === PLAYING_WITH.ANOTHER_PLAYER
                                ? !!playerNameErrorMessage
                                : false
                        }
                        helperText={
                            playingWith === PLAYING_WITH.ANOTHER_PLAYER
                                ? playerNameErrorMessage
                                : ""
                        }
                    />
                </Grid>
            </Box>
        );
    };

    getPlayWithInfoComponent = () => {
        const { playingWith, isPlayerInfoEditable } = this.state;

        return (
            <Box marginX={10} minWidth={150} marginTop={2}>
                <InputLabel id="play-with-input-label">Play with</InputLabel>
                <Select
                    id="play-with-select"
                    labelId="play-with-input-label"
                    fullWidth
                    value={playingWith}
                    onChange={this.setPlayingWith}
                    disabled={!isPlayerInfoEditable}
                >
                    <MenuItem value={PLAYING_WITH.COMPUTER}>Computer</MenuItem>
                    <MenuItem value={PLAYING_WITH.ANOTHER_PLAYER}>
                        Another Player
                    </MenuItem>
                </Select>
            </Box>
        );
    };

    getButtonComponent = () => {
        const { isPlayerInfoEditable } = this.state;

        return (
            <Box marginY={1}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={this.onButtonClick}
                    disabled={this.isButtonDisable()}
                >
                    {isPlayerInfoEditable ? "Play Now" : "Edit Game"}
                </Button>
            </Box>
        );
    };

    render() {
        const playerNameErrorMessage = this.validatePlayerName();

        return (
            <div>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="flex-start"
                >
                    {this.getPlayerOneInfoComponent({ playerNameErrorMessage })}
                    {this.getPlayWithInfoComponent()}
                    {this.getPlayerTwoInfoComponent({ playerNameErrorMessage })}
                </Grid>

                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    {this.getButtonComponent()}
                </Grid>
            </div>
        );
    }
}
