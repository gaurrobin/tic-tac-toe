import { Box, Button, Grid, Typography } from "@material-ui/core";
import React from "react";
import { COLOR, MATRIX, RESULT } from "../../constants/Constant";

export default class PlayerTurn extends React.Component {
    onPlayAgainClick = () => {
        const { onPlayAgain } = this.props;
        onPlayAgain();
    };

    getPlayerTurnComponent = () => {
        const { playerOneName, playerTwoName, playerTurn } = this.props;

        if (playerTurn === MATRIX.PLAYER_ONE) {
            return (
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Box
                        width={30}
                        height={30}
                        bgcolor={COLOR.PLAYER_ONE}
                        marginRight={2}
                    />
                    <Typography>
                        <b>{playerOneName}'s Turn</b>
                    </Typography>
                </Grid>
            );
        } else if (playerTurn === MATRIX.PLAYER_TWO) {
            return (
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Box
                        width={30}
                        height={30}
                        bgcolor={COLOR.PLAYER_TWO}
                        marginRight={2}
                    />
                    <Typography>
                        <b>{playerTwoName}'s Turn</b>
                    </Typography>
                </Grid>
            );
        }
        return <div />;
    };

    getMatchResultCompnent = () => {
        const { playerOneName, playerTwoName, matchResult } = this.props;

        return (
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
            >
                {matchResult === RESULT.PLAYER_ONE_WIN && (
                    <Box
                        width={30}
                        height={30}
                        bgcolor={COLOR.PLAYER_ONE}
                        marginRight={2}
                    />
                )}
                {matchResult === RESULT.PLAYER_TWO_WIN && (
                    <Box
                        width={30}
                        height={30}
                        bgcolor={COLOR.PLAYER_TWO}
                        marginRight={2}
                    />
                )}
                <Typography>
                    {matchResult === RESULT.GAME_DRAW && <b>Match Draw!!!</b>}
                    {matchResult === RESULT.PLAYER_ONE_WIN && (
                        <b>{playerOneName} Win!!!</b>
                    )}
                    {matchResult === RESULT.PLAYER_TWO_WIN && (
                        <b>{playerTwoName} Win!!!</b>
                    )}
                </Typography>
                <Box marginLeft={1}>
                    <Button
                        variant="contained"
                        color="secondary"
                        size="small"
                        onClick={this.onPlayAgainClick}
                    >
                        Play again?
                    </Button>
                </Box>
            </Grid>
        );
    };

    render() {
        const { isGameInProgress } = this.props;

        return (
            <Box marginY={2}>
                {isGameInProgress
                    ? this.getPlayerTurnComponent()
                    : this.getMatchResultCompnent()}
            </Box>
        );
    }
}
