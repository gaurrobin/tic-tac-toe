import { Grid } from "@material-ui/core";
import React from "react";
import { getBestMove } from "../../ai/AI";
import { MATRIX, PLAYING_WITH } from "../../constants/Constant";
import { setTimeOut } from "../../helper/helper";
import Cell from "./Cell";

export default class Matrix extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMoveInProgress: false,
        };
    }

    async componentDidUpdate() {
        const { playingWith } = this.props;
        if (playingWith === PLAYING_WITH.COMPUTER) {
            await this.playAIMove();
        }
    }

    onClick = (index) => {
        const { updatePlayersMove } = this.props;
        const { isMoveInProgress } = this.state;

        if (!isMoveInProgress) {
            this.setState({ isMoveInProgress: true });
            updatePlayersMove({ index });
            this.setState({ isMoveInProgress: false });
        }
    };

    playAIMove = async () => {
        const {
            data: { matrix, isGameInProgress, playerTurn },
            updatePlayersMove,
        } = this.props;
        const { isMoveInProgress } = this.state;

        if (
            !isMoveInProgress &&
            isGameInProgress &&
            playerTurn === MATRIX.PLAYER_TWO
        ) {
            this.setState({ isMoveInProgress: true });
            await setTimeOut(500);
            const bestMove = getBestMove(matrix);
            updatePlayersMove({ index: bestMove });
            this.setState({ isMoveInProgress: false });
        }
    };

    getCell = (index) => {
        const {
            data: { matrix },
        } = this.props;

        return (
            <Cell index={index} value={matrix[index]} onClick={this.onClick} />
        );
    };

    render() {
        return (
            <div>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        {this.getCell(0)}
                        {this.getCell(1)}
                        {this.getCell(2)}
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        {this.getCell(3)}
                        {this.getCell(4)}
                        {this.getCell(5)}
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        {this.getCell(6)}
                        {this.getCell(7)}
                        {this.getCell(8)}
                    </Grid>
                </Grid>
            </div>
        );
    }
}
