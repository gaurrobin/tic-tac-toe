import { Box } from "@material-ui/core";
import React from "react";
import { MATRIX } from "../../constants/Constant";

export default class Cell extends React.Component {
    onClick = () => {
        const { index, onClick, value } = this.props;
        if (value === MATRIX.NONE) {
            onClick(index);
        }
    };

    render() {
        const { index, value } = this.props;

        let bgcolor = "white";
        if (value === MATRIX.PLAYER_ONE) {
            bgcolor = "red";
        } else if (value === MATRIX.PLAYER_TWO) {
            bgcolor = "blue";
        }

        return (
            <Box
                borderLeft={index % 3 === 0 ? 2 : 0}
                borderRight={2}
                borderTop={index < 3 ? 2 : 0}
                borderBottom={2}
                padding={0.3}
            >
                <Box
                    width={55}
                    height={55}
                    bgcolor={bgcolor}
                    onClick={this.onClick}
                />
            </Box>
        );
    }
}
