import React from "react";
import { MATRIX, RESULT } from "../../constants/Constant";
import { isResultDeclared } from "../../helper/helper";
import Matrix from "./Matrix";
import PlayerTurn from "./PlayerTurn";

export default class Board extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            matrix: [],
            playerTurn: MATRIX.NONE,
            isGameInProgress: false,
            matchResult: null,
            totalMoves: 0,
        };
    }

    componentDidMount() {
        this.resetBoard();
    }

    resetBoard = () => {
        const randomValue = Math.random();

        this.setState({
            matrix: Array(9).fill(MATRIX.NONE),
            playerTurn:
                randomValue < 0.5 ? MATRIX.PLAYER_ONE : MATRIX.PLAYER_TWO,
            isGameInProgress: true,
            matchResult: null,
            totalMoves: 0,
        });
    };

    onPlayAgain = () => {
        this.resetBoard();
    };

    updatePlayersMove = (data) => {
        const { index } = data;
        const { updateScore } = this.props;
        const { isGameInProgress, playerTurn, matrix, totalMoves } = this.state;

        if (isGameInProgress && matrix[index] === MATRIX.NONE) {
            let updatedPlayerTurn = MATRIX.NONE;
            let updatedMatrix = [...matrix];
            const updatedTotalMoves = totalMoves + 1;

            if (playerTurn === MATRIX.PLAYER_ONE) {
                updatedPlayerTurn = MATRIX.PLAYER_TWO;
                updatedMatrix[index] = playerTurn;
            } else if (playerTurn === MATRIX.PLAYER_TWO) {
                updatedPlayerTurn = MATRIX.PLAYER_ONE;
                updatedMatrix[index] = playerTurn;
            }

            this.setState({
                playerTurn: updatedPlayerTurn,
                matrix: updatedMatrix,
                totalMoves: updatedTotalMoves,
            });

            const isDeclared = isResultDeclared(
                updatedMatrix,
                updatedTotalMoves
            );

            if (isDeclared) {
                if (updatedTotalMoves > 8) {
                    this.setState({
                        isGameInProgress: false,
                        matchResult: RESULT.GAME_DRAW,
                    });
                    updateScore(RESULT.GAME_DRAW);
                } else if (playerTurn === MATRIX.PLAYER_ONE) {
                    this.setState({
                        isGameInProgress: false,
                        matchResult: RESULT.PLAYER_ONE_WIN,
                    });
                    updateScore(RESULT.PLAYER_ONE_WIN);
                } else if (playerTurn === MATRIX.PLAYER_TWO) {
                    this.setState({
                        isGameInProgress: false,
                        matchResult: RESULT.PLAYER_TWO_WIN,
                    });
                    updateScore(RESULT.PLAYER_TWO_WIN);
                }
            }
        }
    };

    getPlayerTurn = () => {
        const { playerOneName, playerTwoName } = this.props;
        const { playerTurn, isGameInProgress, matchResult } = this.state;

        return (
            <PlayerTurn
                playerOneName={playerOneName}
                playerTwoName={playerTwoName}
                playerTurn={playerTurn}
                isGameInProgress={isGameInProgress}
                matchResult={matchResult}
                onPlayAgain={this.onPlayAgain}
            />
        );
    };

    getGameBoard = () => {
        const { playingWith } = this.props;
        return (
            <Matrix
                data={this.state}
                updatePlayersMove={this.updatePlayersMove}
                playingWith={playingWith}
            />
        );
    };

    render() {
        return (
            <div>
                {this.getPlayerTurn()}
                {this.getGameBoard()}
            </div>
        );
    }
}
