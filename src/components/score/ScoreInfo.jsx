import { Box, Grid, Typography } from "@material-ui/core";
import React from "react";
import { COLOR } from "../../constants/Constant";

export default class ScoreInfo extends React.Component {
    getScoreHeadingComponent = () => {
        return (
            <Box marginTop={1}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Typography style={{ fontSize: 30 }}>
                        <b>SCORE</b>
                    </Typography>
                </Grid>
            </Box>
        );
    };

    getMatchPlayedComponent = () => {
        const { gameInfo: { totalMatches } = {} } = this.props;

        return (
            <Box marginTop={1}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Typography>
                        Total Matches Played: <b>{totalMatches}</b>
                    </Typography>
                </Grid>
            </Box>
        );
    };

    getPlayerScoreComponent = () => {
        const {
            gameInfo: {
                playerOneName,
                playerTwoName,
                playerOneScore,
                playerTwoScore,
            } = {},
        } = this.props;

        return (
            <Box marginTop={1}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Box
                        width={30}
                        height={30}
                        bgcolor={COLOR.PLAYER_ONE}
                        marginRight={2}
                    />
                    <Typography>{playerOneName}</Typography>
                    <Box marginX={5}>
                        <Box display="inline" color={COLOR.PLAYER_ONE}>
                            <Typography
                                display="inline"
                                style={{ fontSize: 20 }}
                            >
                                <b>{playerOneScore}</b>
                            </Typography>
                        </Box>

                        <Typography display="inline" style={{ fontSize: 20 }}>
                            <b> - </b>
                        </Typography>

                        <Box display="inline" color={COLOR.PLAYER_TWO}>
                            <Typography
                                display="inline"
                                style={{ fontSize: 20 }}
                            >
                                <b>{playerTwoScore}</b>
                            </Typography>
                        </Box>
                    </Box>

                    <Box
                        width={30}
                        height={30}
                        bgcolor={COLOR.PLAYER_TWO}
                        marginRight={2}
                    />
                    <Typography>{playerTwoName}</Typography>
                </Grid>
            </Box>
        );
    };

    render() {
        return (
            <div>
                {this.getScoreHeadingComponent()}
                {this.getMatchPlayedComponent()}
                {this.getPlayerScoreComponent()}
            </div>
        );
    }
}
