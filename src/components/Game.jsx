import { Box } from "@material-ui/core";
import React from "react";
import { RESULT } from "../constants/Constant";
import Board from "./board/Board";
import PlayersInfo from "./players/PlayersInfo";
import ScoreInfo from "./score/ScoreInfo";

export default class Game extends React.Component {
    constructor() {
        super();
        this.state = {
            playerOneName: "",
            playerTwoName: "",
            playingWith: "",
            showBoard: false,
            totalMatches: 0,
            playerOneScore: 0,
            playerTwoScore: 0,
        };
    }

    onPlayNowClick = (data) => {
        const { playerOneName, playerTwoName, playingWith } = data;
        this.setState({
            playerOneName,
            playerTwoName,
            playingWith,
            showBoard: true,
        });
    };

    onEditGameClick = (data) => {
        const { playerOneName, playerTwoName, playingWith } = data;
        this.setState({
            playerOneName,
            playerTwoName,
            playingWith,
            showBoard: false,
            totalMatches: 0,
            playerOneScore: 0,
            playerTwoScore: 0,
        });
    };

    updateScore = (result) => {
        const { totalMatches, playerOneScore, playerTwoScore } = this.state;
        if (result === RESULT.PLAYER_ONE_WIN) {
            this.setState({
                totalMatches: totalMatches + 1,
                playerOneScore: playerOneScore + 1,
            });
        } else if (result === RESULT.PLAYER_TWO_WIN) {
            this.setState({
                totalMatches: totalMatches + 1,
                playerTwoScore: playerTwoScore + 1,
            });
        } else if (result === RESULT.GAME_DRAW) {
            this.setState({
                totalMatches: totalMatches + 1,
            });
        }
    };

    render() {
        const {
            showBoard,
            playerOneName,
            playerTwoName,
            playingWith,
        } = this.state;

        return (
            <Box padding={1} border={3}>
                <PlayersInfo
                    onPlay={this.onPlayNowClick}
                    onEdit={this.onEditGameClick}
                />
                {showBoard && <ScoreInfo gameInfo={this.state} />}
                {showBoard && (
                    <Board
                        playerOneName={playerOneName}
                        playerTwoName={playerTwoName}
                        playingWith={playingWith}
                        updateScore={this.updateScore}
                    />
                )}
            </Box>
        );
    }
}
