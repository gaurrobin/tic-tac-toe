import { MATRIX } from "../constants/Constant";
import { DEFAULT_SCORE } from "./Constant";

const isMovesLeft = (matrix) => {
    for (let i = 0; i < 9; i++) {
        if (matrix[i] === MATRIX.NONE) {
            return true;
        }
    }
    return false;
};

const getDefaultScoreByPlayer = (player) => {
    if (player === MATRIX.PLAYER_ONE) {
        return DEFAULT_SCORE.MIN;
    } else if (player === MATRIX.PLAYER_TWO) {
        return DEFAULT_SCORE.MAX;
    }
    return null;
};

const getWinningScore = (matrix) => {
    // if row is crossed
    for (let i = 0; i < 9; i = i + 3) {
        if (matrix[i] === matrix[i + 1] && matrix[i + 1] === matrix[i + 2]) {
            const score = getDefaultScoreByPlayer(matrix[i]);
            if (score) {
                return score;
            }
        }
    }

    // if column is crossed
    for (let i = 0; i < 3; i++) {
        if (matrix[i] === matrix[i + 3] && matrix[i + 3] === matrix[i + 6]) {
            const score = getDefaultScoreByPlayer(matrix[i]);
            if (score) {
                return score;
            }
        }
    }

    // if diagonal is crosses
    if (matrix[0] === matrix[4] && matrix[4] === matrix[8]) {
        const score = getDefaultScoreByPlayer(matrix[4]);
        if (score) {
            return score;
        }
    } else if (matrix[2] === matrix[4] && matrix[4] === matrix[6]) {
        const score = getDefaultScoreByPlayer(matrix[4]);
        if (score) {
            return score;
        }
    }

    return DEFAULT_SCORE.NONE;
};

const miniMax = (matrix, depth, isMaximizer) => {
    if (isMaximizer) {
        let bestScore = Number.NEGATIVE_INFINITY;
        for (let i = 0; i < 9; i++) {
            if (matrix[i] === MATRIX.NONE) {
                matrix[i] = MATRIX.PLAYER_TWO;
                const score = getMiniMaxScore(matrix, depth + 1, !isMaximizer);
                bestScore = Math.max(bestScore, score);
                matrix[i] = MATRIX.NONE;
            }
        }
        return bestScore;
    } else {
        let bestScore = Number.POSITIVE_INFINITY;
        for (let i = 0; i < 9; i++) {
            if (matrix[i] === MATRIX.NONE) {
                matrix[i] = MATRIX.PLAYER_ONE;
                const score = getMiniMaxScore(matrix, depth + 1, !isMaximizer);
                bestScore = Math.min(bestScore, score);
                matrix[i] = MATRIX.NONE;
            }
        }
        return bestScore;
    }
};

const getMiniMaxScore = (matrix, depth, isMaximizer) => {
    let score = getWinningScore(matrix);
    if (score === DEFAULT_SCORE.MIN) {
        return score + depth;
    } else if (score === DEFAULT_SCORE.MAX) {
        return score - depth;
    } else if (!isMovesLeft(matrix)) {
        return DEFAULT_SCORE.NONE;
    }
    return miniMax(matrix, depth, isMaximizer);
};

const getBestMove = (matrix) => {
    let bestScore = Number.NEGATIVE_INFINITY;
    let bestMove = null;

    for (let i = 0; i < 9; i++) {
        if (matrix[i] === MATRIX.NONE) {
            matrix[i] = MATRIX.PLAYER_TWO;
            const score = getMiniMaxScore(matrix, 0, false);
            matrix[i] = MATRIX.NONE;

            if (bestScore < score) {
                bestScore = score;
                bestMove = i;
            }
        }
    }
    return bestMove;
};

export { getBestMove };
