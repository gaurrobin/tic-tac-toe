const DEFAULT_SCORE = {
    NONE: 0,
    MAX: 10,
    MIN: -10,
};

export { DEFAULT_SCORE };
