import { MATRIX } from "../constants/Constant";

const isRowCrossed = (matrix) => {
    for (let i = 0; i < 9; i = i + 3) {
        if (
            matrix[i] !== MATRIX.NONE &&
            matrix[i] === matrix[i + 1] &&
            matrix[i + 1] === matrix[i + 2]
        ) {
            return true;
        }
    }
    return false;
};

const isColumnCrossed = (matrix) => {
    for (let i = 0; i < 3; i++) {
        if (
            matrix[i] !== MATRIX.NONE &&
            matrix[i] === matrix[i + 3] &&
            matrix[i + 3] === matrix[i + 6]
        ) {
            return true;
        }
    }
    return false;
};

const isDiagonalCrossed = (matrix) => {
    if (
        matrix[0] !== MATRIX.NONE &&
        matrix[0] === matrix[4] &&
        matrix[4] === matrix[8]
    ) {
        return true;
    } else if (
        matrix[2] !== MATRIX.NONE &&
        matrix[2] === matrix[4] &&
        matrix[4] === matrix[6]
    ) {
        return true;
    }
    return false;
};

const isResultDeclared = (matrix, totalMoves) => {
    if (totalMoves < 5) {
        return false;
    } else if (totalMoves === 9) {
        return true;
    }

    return (
        isRowCrossed(matrix) ||
        isColumnCrossed(matrix) ||
        isDiagonalCrossed(matrix)
    );
};

const setTimeOut = (timeInMS) => {
    const promise = new Promise((resolve) => {
        setTimeout(resolve, timeInMS);
    });
    return promise;
};

export { isResultDeclared, setTimeOut };
